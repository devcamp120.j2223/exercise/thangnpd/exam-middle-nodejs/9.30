const express = require('express');
const app = express();
const port = 8000;

//Import router
const userInRouter = require('./app/routes/userRouter');

app.use('/', userInRouter); // Request sign in chạy về router này

app.listen(port, () => {
  console.log(`9.10 app listening on port ${port}`);
})