//Import router
const { request, response } = require('express');
const express = require('express');
const router = express.Router();

//Import data User
const { UserClassList } = require('../../data');

//Get method with age query
router.get('/users', (request, response) => {
  // B1: Thu thập dữ liệu
  let query = request.query.age; // ?age=
  console.log(`query age: ${query}`);

  // B2: Kiểm tra dữ liệu
  if (query == undefined) { //nếu age không có giá trị thì response sẽ trả về toàn bộ user
    response.status(404).json({
      UserClassList
    })
  }

  if(isNaN(query)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "age is not a number"
    })
  }
  
  if ((Number.isInteger(query) && query > 0)) { //nếu age ko phải là integer và age < 0 thì lỗi 400
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "age is not valid"
    })
  }
  //B3: Lọc dữ liệu
  let filterUserByAge = [];
  for (var i = 0; i < UserClassList.length; i++) {
    if (UserClassList[i].age > query) {
      filterUserByAge.push(UserClassList[i]);
    }
  }
  if (filterUserByAge.length > 0) {
    response.status(200).json({
      filterUserByAge
    })
  } else {
    response.status(404).json({
      UserClassList
    })
  }
})

//Get method with params
router.get('/users/:userId', (request, response) => {
  // B1: Thu thập dữ liệu
  let userId = request.params.userId;
  console.log(`param ID: ${userId}`);
  // B2: Kiểm tra dữ liệu
  if (Number.isInteger(userId) && userId < 0) { //nếu userId là integer và userId < 0 thì lỗi 400
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "userId is not valid"
    })
  }

  if(isNaN(userId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "userId is not a number"
    })
  }
  
  // B3: lọc dữ liệu
  for (var i = 0; i < UserClassList.length; i++) {
    if (UserClassList[i].id == userId) {
      response.status(200).json({
        data: UserClassList[i]
      })
    }
  }
  return response.status(404).json({
    UserClassList
  })
})

router.post('/users', (request, response) => {
  response.status(200).json({
    message: `Method: ${request.method}`
  })
})

router.put('/users', (request, response) => {
  response.status(200).json({
    message: `Method: ${request.method}`
  })
})

module.exports = router; //Export router để call main app ở file index.js