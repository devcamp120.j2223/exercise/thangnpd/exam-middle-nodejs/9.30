//Khai báo class User với các thuộc tính
const User = class {
  constructor(id, name, position, office, age, startDate) {
    this.id = id;
    this.name = name;
    this.position = position;
    this.office = office;
    this.age = age;
    this.startDate = startDate;
  }
}

//khởi tạo 10 thực thể user nhờ dữ liệu đã cho
var userObj = [
  {
    user: [1, "Airi Satou", "Accountant", "Tokyo", 33, "2008/11/28"]
  },
  {
    user: [2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", 47, "2009/10/09"]
  },
  {
    user: [3, "Ashton Cox", "Junior Technical Author", "San Francisco", 66, "2009/01/12"]
  },
  {
    user: [4, "Bradley Greer", "Software Engineer", "London", 41, "2012/10/13"]
  },
  {
    user: [5, "Brenden Wagner", "Software Engineer", "San Francisco", 28, "2011/06/07"]
  },
  {
    user: [6, "Brielle Williamson", "Integration Specialist", "New York", 61, "2012/12/02"]
  },
  {
    user: [7, "Bruno Nash", "Software Engineer", "London", 38, "2011/05/03"]
  },
  {
    user: [8, "Caesar Vance", "Pre-Sales Support", "New York", 21, "2011/12/12"]
  },
  {
    user: [9, "Cara Stevens", "Sales Assistant", "New York", 46, "2011/12/06"]
  },
  {
    user: [10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", 22, "2012/03/29"]
  }
]

var UserClassList = [];

for(var i = 0; i < userObj.length; i++) {
  let eachUser = userObj[i].user;
  UserClassList.push(new User(eachUser[0], eachUser[1], eachUser[2], eachUser[3], eachUser[4], eachUser[5], eachUser[6]));
}

module.exports = {UserClassList};